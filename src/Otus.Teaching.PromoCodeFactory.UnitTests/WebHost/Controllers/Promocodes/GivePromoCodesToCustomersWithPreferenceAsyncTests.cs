﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Promocodes
{
    public class GivePromoCodesToCustomersWithPreferenceAsyncTests
    {
        private readonly Mock<IRepository<PromoCode>> _promoCodesRepositoryMock;
        private readonly Mock<IRepository<Employee>> _employeeRepositoryMock;
        private readonly Mock<IRepository<Preference>> _preferenceRepositoryMock;
        private readonly Mock<IRepository<Customer>> _customerRepositoryMock;
        private readonly PromocodesController _promoCodesController;

        public GivePromoCodesToCustomersWithPreferenceAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _promoCodesRepositoryMock = fixture.Freeze<Mock<IRepository<PromoCode>>>();
            _employeeRepositoryMock = fixture.Freeze<Mock<IRepository<Employee>>>();
            _preferenceRepositoryMock = fixture.Freeze<Mock<IRepository<Preference>>>();
            _customerRepositoryMock = fixture.Freeze<Mock<IRepository<Customer>>>();
            _promoCodesController = fixture.Build<PromocodesController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GivePromoCodesToCustomersWithPreferenceAsync_AddPromoCode_CustomersWithPromoCodePreferences()
        {
            //Arrange
            var promoCode = new PromoCodeBuilder()
                .WithCreateBasePromoCode()
                .Build();
            var promoCodesAdded = new List<PromoCode>();
            _promoCodesRepositoryMock.Setup(i => i.AddAsync(It.IsAny<PromoCode>()))
                .Callback((PromoCode promoCode) => promoCodesAdded.Add(promoCode));

            var employee = new EmployeeBuilder()
                .WhithCreateBaseEmployee()
                .Build();
            var empoyeeId = employee.Id;
            _employeeRepositoryMock.Setup(repo => repo.GetByIdAsync(empoyeeId))
                .ReturnsAsync(employee);

            var preferences = new PreferencesBuilder()
                .WhithCreateBasePreferences()
                .Build();
            _preferenceRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(preferences);

            var customer = new CustomerBuilder()
                .WhithCreateBaseCustomer()
                .Build();
            _customerRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(new List<Customer> { customer });
            

            var request = new GivePromoCodeRequest()
            {
                PromoCode = promoCode.Code,
                BeginDate = promoCode.BeginDate,
                EndDate = promoCode.EndDate,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerName = promoCode.PartnerName,
                Preference = promoCode.Preference.Name,
                PartnerManagerId = empoyeeId
            };

            //Act
            var result = await _promoCodesController.GivePromoCodesToCustomersWithPreferenceAsync(request);

            //Assert
            _promoCodesRepositoryMock.Verify(r => r.AddAsync(promoCodesAdded[0]));
            var customerResult = Assert.IsType<ActionResult<List<CustomerShortResponse>>>(result);
            var customerOkResult = Assert.IsType<OkObjectResult>(customerResult.Result);
            customerOkResult.Value.Should().NotBeNull();
            var customers = Assert.IsType<List<CustomerShortResponse>>(customerOkResult.Value);
            Assert.Single(customers);
            Assert.Equal(customer.Id, customers[0].Id);
        }
    }
}
