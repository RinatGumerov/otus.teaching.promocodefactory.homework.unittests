﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Promocodes
{
    public class EmployeeBuilder
    {
        private Employee _employee;

        public EmployeeBuilder()
        {
            
        }

        public EmployeeBuilder WhithCreateBaseEmployee()
        {
            _employee = new Employee
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Role = new Role
                {
                    Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                    Name = "Admin",
                    Description = "Администратор"
                },
                AppliedPromocodesCount = 10
            };

            return this;
        }

        public Employee Build()
        {
            return _employee;
        }
    }
}
